# Instalación

### Características del frontend

Este proyecto esta construido sobre el framework NextJs v14.\*. Algunas dependencias agregadas:

- Next Auth
- React Query
- MUI
- TailwindCss
- Docker

### Pasos para instalar el proyecto

1. Clonar el proyecto del repositorio

2. Ejecutar el comando `npm install`

3. Copiar el archivo `.env-template` y reemplazar el nombre con `.env`.

4. Cambiar las variables de entorno del archivo `.env` con la siguiente informacion

```
API_OBSERVADOR=http://localhost:9000/api

NEXTAUTH_SECRET=12345
```

6. Levantar el proyecto

```
npm run dev
```

7. Usuario administrador

```
usuario: admin@gmail.com
password: admin
```
