import { Box, Container } from "@mui/material";

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <Container>
      <Box sx={{ bgcolor: "white", height: "100vh" }}>{children}</Box>
    </Container>
  );
}
