import {
  Avatar,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import React from "react";
import { Edit } from "@mui/icons-material";

const getNews = async (limit = 20, offset = 0) => {
  const data = await fetch(`${process.env.API_OBSERVADOR}/news`).then((res) =>
    res.json()
  );
  return data;
};

const NewsAdmin = async () => {
  const notices = await getNews();
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Titulo</TableCell>
            <TableCell align="right">Autor</TableCell>
            <TableCell align="right">Fecha</TableCell>
            <TableCell align="right">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {notices.map((row) => (
            <TableRow
              key={row.name}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                <Avatar>
                  <img src={row.image} alt="" srcset="" />
                </Avatar>
                {row.title}
              </TableCell>
              <TableCell align="right">{row.user.fullName}</TableCell>
              <TableCell align="right">{row.created_at}</TableCell>
              <TableCell align="right">
                <IconButton aria-label="delete">
                  <DeleteIcon />
                </IconButton>
                <IconButton aria-label="delete">
                  <Edit />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default NewsAdmin;
