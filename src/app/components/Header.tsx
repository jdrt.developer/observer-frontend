"use client";

import { useState } from "react";

import {
  AppBar,
  Avatar,
  Box,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
  Container,
  Button,
  TextField,
  InputAdornment,
} from "@mui/material";
import { Search } from "@mui/icons-material";
import MainMenu from "./Menu/MainMenu";
import Link from "next/link";

const settings = ["Profile", "Account", "Dashboard", "Logout"];

const Header = () => {
  const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null);

  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <AppBar position="relative" elevation={0} color="transparent">
      <Container maxWidth="lg" className="px-0">
        <Toolbar disableGutters variant="dense">
          <Box
            sx={{
              display: { xs: "flex", md: "flex" },
              padding: ".3em",
            }}
            alignItems="center"
            component="section"
            gap={2}
          >
            <Link href="" className="uppercase" style={{ color: "#e30303" }}>
              INICIO
            </Link>
            |
            <Link href="" className="uppercase" style={{ color: "#e30303" }}>
              CONTACTO
            </Link>
            |
            <Link href="" className="uppercase" style={{ color: "#e30303" }}>
              ACERCA DE
            </Link>
          </Box>
          <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}></Box>
          <Box sx={{ flexGrow: 0 }}>
            <IconButton
              onClick={handleOpenUserMenu}
              sx={{ p: 0 }}
              className="ml-3"
            >
              <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg" />
            </IconButton>
            <Menu
              sx={{ mt: "65px" }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              {settings.map((setting) => (
                <MenuItem key={setting} onClick={handleCloseUserMenu}>
                  <Typography textAlign="center">{setting}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
        </Toolbar>
        <Toolbar disableGutters className="mt-6">
          <Typography
            variant="h4"
            noWrap
            component="a"
            href="#app-bar-with-responsive-menu"
            sx={{
              mr: 5,
              display: { xs: "none", md: "flex" },
              fontFamily: "Times New Roman",
              fontWeight: "bold",
              letterSpacing: ".1rem",
              color: "inherit",
              textDecoration: "none",
            }}
          >
            EL OBSERVADOR
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}></Box>
          <Box sx={{ flexGrow: 0 }}>
            <TextField
              id="input-with-icon-textfield"
              placeholder="Buscar noticia"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Search />
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              size="small"
            />
          </Box>
        </Toolbar>
        <Toolbar
          sx={{ bgcolor: "#292929", color: "#fffff", height: "40px" }}
          className="my-6"
          variant="dense"
        >
          <MainMenu />
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default Header;
