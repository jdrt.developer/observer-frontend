import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";

export default function MainCard() {
  return (
    <Card className="mb-4" elevation={0} square={true}>
      <CardMedia
        sx={{ height: 200 }}
        image="https://mui.com/static/images/cards/contemplative-reptile.jpg"
        title="green iguana"
      />
      <CardContent>
        <Typography
          gutterBottom
          variant="h5"
          component="div"
          textAlign="justify"
        >
          Lizard sadasdd dsad sasada s dsadsa
        </Typography>
        <Typography
          gutterBottom
          variant="body1"
          component="div"
          color="text.secondary"
          textAlign="justify"
        >
          Posted on Juanary 7, 2024 by admin
        </Typography>
        <Typography variant="body2" textAlign="justify">
          Lizards are a widespread group of squamate reptiles, with over 6,000
          species, ranging across all continents except Antarctica Lizards are a
          widespread group of squamate reptiles, with over 6,000 species,
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">Continuar leyendo</Button>
      </CardActions>
    </Card>
  );
}
