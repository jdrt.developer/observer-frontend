import { Box, Stack } from "@mui/material";
import MenuItem from "./MenuItem";
interface Page {
  label: string;
  path: string;
}
const MainMenu = () => {
  const pages = [
    {
      label: "Inicio",
      path: "/",
    },
    {
      label: "Noticias",
      path: "/news",
    },
    {
      label: "About",
      path: "/about",
    },
  ];

  return (
    <Box
      sx={{
        display: { xs: "flex", md: "flex" },
        padding: ".1em .1em",
      }}
      alignItems="center"
      component="section"
      gap={4}
    >
      {pages.map((page: Page) => (
        <MenuItem label={page.label} path={page.path} key={page.path} />
      ))}
    </Box>
  );
};

export default MainMenu;
