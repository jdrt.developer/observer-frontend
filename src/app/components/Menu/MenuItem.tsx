import Link from "next/link";
interface Page {
  label: string;
  path: string;
}

const MenuItem = ({ label, path }: Page) => {
  return (
    <Link key={path} href={path} className=" text-white">
      {label}
    </Link>
  );
};

export default MenuItem;
