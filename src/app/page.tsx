import {
  Avatar,
  Button,
  Card,
  CardContent,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@mui/material";
import MainCard from "./components/MainCard";

const getResents = async () => {
  const data = await fetch(`${process.env.API_OBSERVADOR}/news/recents`).then(
    (res) => res.json()
  );

  // const pokemons = data.results.map((pokemon) => ({
  //   id: pokemon.url.split("/").at(-2)!,
  //   name: pokemon.name,
  // }));

  return data;
};

const Home = async () => {
  const news = await getResents();
  console.log(news);
  return (
    <Grid container spacing={2} className=" p-4">
      <Grid xs={12} md={8}>
        <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
          <Grid item xs={6}>
            <MainCard />
          </Grid>
          <Grid item xs={6}>
            <MainCard />
          </Grid>
          <Grid item xs={6}>
            <MainCard />
          </Grid>
          <Grid item xs={6}>
            <MainCard />
          </Grid>
        </Grid>
      </Grid>
      <Grid xs={12} md={4} className="p-4">
        <Card className="mb-4" elevation={0} square={true}>
          <CardContent></CardContent>
        </Card>
        <List sx={{ width: "100%", bgcolor: "background.paper" }}>
          <ListItem alignItems="flex-start">
            <ListItemAvatar className="mr-2">
              <Avatar
                alt="Remy Sharp"
                variant="rounded"
                src="https://mui.com/static/images/cards/contemplative-reptile.jpg"
                sx={{ width: 50, height: 50 }}
              />
            </ListItemAvatar>
            <ListItemText
              primary="Brunch this weekend?"
              secondary={
                <>
                  <Typography
                    sx={{ display: "inline" }}
                    component="span"
                    variant="body2"
                    color="text.primary"
                  >
                    Ali Connors
                  </Typography>
                  {" — I'll be in your neighborhood doing errands this…"}
                </>
              }
            />
          </ListItem>
          <Divider variant="inset" component="li" />
        </List>
      </Grid>
    </Grid>
  );
};

export default Home;
